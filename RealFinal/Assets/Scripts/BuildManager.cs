﻿
using UnityEngine;

public class BuildManager : MonoBehaviour
{
    public static BuildManager instance;


     void Awake()
    {
        instance = this;
    }


    public GameObject standardTurretPrefab;
    public GameObject missileLauncherPrefab;
    public GameObject sniperTurretPrefab;

    public GameObject buildEffect;

    private TurretBluePrint turretToBuild;
    public bool CanBuild { get { return turretToBuild != null; } }
    public bool HasMoney { get { return PlayerStats.Money >= turretToBuild.cost;} }


    public void BuildTurretOn(Node node)
    {
        if (PlayerStats.Money < turretToBuild.cost)
        {
            Debug.Log("Not Enough Money");
            return;
        }


        PlayerStats.Money -= turretToBuild.cost;    


        GameObject turret = (GameObject)Instantiate(turretToBuild.prefab, node.GetBuildPosition(), Quaternion.identity);
        node.turret = turret;

        GameObject effect = (GameObject) Instantiate(buildEffect, node.GetBuildPosition(), Quaternion.identity);
        Destroy(effect, 5f);
        //plays build effect and destorys it
        Debug.Log("Turret Built! Money Left: " + PlayerStats.Money);

    }
    public void SelectTurretToBuild (TurretBluePrint turret)
    {
        turretToBuild = turret;
    }

}
