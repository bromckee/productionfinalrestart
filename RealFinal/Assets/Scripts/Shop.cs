﻿
using UnityEngine;

public class Shop : MonoBehaviour
{
    public TurretBluePrint standardTurret;
    public TurretBluePrint missileLauncher;
    public TurretBluePrint sniperTurret;
    BuildManager buildManager;



    private void Start()
    {
        buildManager = BuildManager.instance;
    }
    public void SelectStandardTurret()
    {
        Debug.Log("StandardTurretPurchased");
        buildManager.SelectTurretToBuild(standardTurret);
    }
    public void SelectMissileLauncher()
    {
        Debug.Log("Missile Launcher Purchased");
        buildManager.SelectTurretToBuild(missileLauncher);
    }
    public void SelectSniperTurret()
    {
        Debug.Log("Sniper Turret Purchased");
        buildManager.SelectTurretToBuild(sniperTurret);
    }

}
